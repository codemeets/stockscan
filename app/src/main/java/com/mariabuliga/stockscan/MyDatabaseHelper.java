package com.mariabuliga.stockscan;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.Date;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    private Context context;
    private static final String DATABASE_NAME = "StockAnalysis.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_NAME = "my analysis";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_COMPANY = "Company";
    private static final String COLUMN_ACQUISITION_DATE = "Acquisition Date";
    private static final String COLUMN_INVESTED_AMOUNT = "Invested Amount";
    private static final String COLUMN_CURRENT_DATE = "Date";
    private static final String COLUMN_CURRENT_VALUE = "Current Value";
    private static final String COLUMN_PRICE_PER_SHARE = "Current Price";
    private static final String COLUMN_PRICE_PER_SHARE_JAN2021 = "Price - Jan2021";
    private static final String COLUMN_PRICE_PER_SHARE_DEC2021 = "Price - end of Dec2021";
    private static final String COLUMN_PRICE_PER_SHARE_MIN2021 = "Price - Min2021";
    private static final String COLUMN_PRICE_PER_SHARE_MAX2021 = "Price - Max2021";
    private static final String COLUMN_MIN_ACQUISITION_PRICE = "Min Acquisition Price";
    private static final String COLUMN_AVERAGE_INVESTMENT_PER_SHARE = "Average Investment per share";
    private static final String COLUMN_CURRENT_PRICE_VS_MIN_ACQUISITION_PRICE1 = "Current Price vs Min Acquisition Price";
    private static final String COLUMN_CURRENT_PRICE_VS_MIN_ACQUISITION_PRICE2 = "Current Price vs Min Acquisition Price (%)";
    private static final String COLUMN_EPS_Price = "EPS/Price (%)";
    private static final String COLUMN_ROA = "ROA (%)";
    private static final String COLUMN_P_E = "P/E";
    private static final String COLUMN_EPS = "EPS";

    public MyDatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query =
                "CREATE TABLE " + TABLE_NAME +
                        " (" + COLUMN_ID +"INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        COLUMN_COMPANY + " TEXT, " +
                        COLUMN_PRICE_PER_SHARE + " INTEGER, " +
                        COLUMN_PRICE_PER_SHARE_JAN2021 + " INTEGER, " +
                        COLUMN_PRICE_PER_SHARE_DEC2021 + " INTEGER, " +
                        COLUMN_PRICE_PER_SHARE_MIN2021 + " INTEGER, " +
                        COLUMN_PRICE_PER_SHARE_MAX2021 + " INTEGER, " +

                        COLUMN_EPS_Price + " INTEGER, " +
                        COLUMN_ROA + " INTEGER, " +
                        COLUMN_P_E + " INTEGER, " +
                        COLUMN_EPS + " INTEGER, " +

                        COLUMN_ACQUISITION_DATE + " DATE, " +
                        COLUMN_INVESTED_AMOUNT + " INTEGER, " +
                        COLUMN_CURRENT_DATE + " DATE, " +
                        COLUMN_CURRENT_VALUE + " INTEGER, " +

                        COLUMN_MIN_ACQUISITION_PRICE + " INTEGER, " +
                        COLUMN_AVERAGE_INVESTMENT_PER_SHARE + " INTEGER, " +
                        COLUMN_CURRENT_PRICE_VS_MIN_ACQUISITION_PRICE1 + " INTEGER, " +
                        COLUMN_CURRENT_PRICE_VS_MIN_ACQUISITION_PRICE2 + " INTEGER);";
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
