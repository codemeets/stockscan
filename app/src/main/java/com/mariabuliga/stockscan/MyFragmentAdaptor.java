package com.mariabuliga.stockscan;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class MyFragmentAdaptor extends FragmentStateAdapter {
    public MyFragmentAdaptor(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch(position){
            case 1:
                return new _2PriceFragment();
            case 2:
                return new _3InvestmentFragment();
            case 3:
                return new _4OpinionFragment();
        }
        return new _1CompanyFragment();
    }

    @Override
    public int getItemCount() {
        return 4;
    }
}
