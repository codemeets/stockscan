package com.mariabuliga.stockscan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button buttonIndicators;
    private Button buttonAnalysis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonIndicators = (Button) findViewById(R.id.btn_stockIndicators);
        buttonIndicators.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openIndicatorsActivity();
            }
        });

        buttonAnalysis = (Button)findViewById(R.id.btn_stockAnalysis);
        buttonAnalysis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openStockAnalysisActivity();
            }
        });
    }

    public void openIndicatorsActivity(){
        Intent intent1 = new Intent(this, IndicatorsActivity.class);
        startActivity(intent1);
    }

    public void openStockAnalysisActivity(){
        Intent intent2 = new Intent(this, StockAnalysisActivity.class);
        startActivity(intent2);
    }


}